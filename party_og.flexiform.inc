<?php
/**
 * @file
 * Party OG implementation of flexiform hooks.
 */

/**
 * Implements hook_flexiform_element_info().
 */
function party_og_flexiform_element_info() {
  $elements = array();

  foreach (field_info_instances('party', 'party') as $instance) {
    $field = field_info_field($instance['field_name']);
    if ($field['type'] == 'entityreference') {
      if (!empty($field['settings']['handler_settings']['behaviors']['og_behavior']['status'])) {
        $elements['party']['party']["party_og:{$field['field_name']}:add_to_group"] = array(
          'label' => t('@label: Add to group', array('@label' => $instance['label'])),
          'class' => 'PartyOgElementAddToGroup',
          'group' => 'Party OG',
          'field_name' => $field['field_name'],
        );
      }
    }
  }

  return $elements;
}
