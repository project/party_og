<?php
/**
 * @file
 * Install a roles field on the og_membership entity.
 */

/**
 * Implements hook_install();
 */
function party_og_install() {
  $t = get_t();
  $entity_info = entity_get_info('og_membership');

  if (!field_info_field('party_og_membership_roles')) {
    $field = array(
      'field_name' => 'party_og_membership_roles',
      'type' => 'list_integer',
      'cardinality' => FIELD_CARDINALITY_UNLIMITED,
      'settings' => array(
        'allowed_values_function' => 'party_og_membership_roles_allowed_values',
      ),
    );
    field_create_field($field);
  }

  foreach (array_keys($entity_info['bundles']) as $bundle) {
    if (!field_info_instance('og_membership', 'party_og_membership_roles', $bundle)) {
      $instance = array(
        'field_name' => 'party_og_membership_roles',
        'entity_type' => 'og_membership',
        'bundle' => $bundle,
        'label' => $t('Membership Roles'),
        'description' => $t('The roles this party has as a member of the group.'),
        'widget' => array(
          'type' => 'options_buttons',
        ),
      );
      field_create_instance($instance);
    }
  }

  // Add party group field to users.
  $field_def = og_fields_info(OG_AUDIENCE_FIELD);
  $field = $field_def['field'];
  $field['field_name'] = 'party_og_user_audience';
  $field['settings']['target_type'] = 'party';
  if (!field_info_field($field['field_name'])) {
    field_create_field($field);
  }

  $instance = $field_def['instance'];
  $instance['field_name'] = 'party_og_user_audience';
  $instance['entity_type'] = 'user';
  $instance['bundle'] = 'user';
  $instance['label'] = t('Party Audience');
  if (!field_info_instance('user', 'party_og_user_audience', 'user')) {
    field_create_instance($instance);
  }

  // Make Parties groups.
  $og_field = og_fields_info('group_group');
  og_create_field('group_group', 'party', 'party', $og_field);
}

/**
 * Implements hook_uninstall().
 */
function party_og_uninstall() {
  $instance = field_info_instance('og_membership', 'party_og_membership_roles', 'og_membership_type_default');
  field_delete_instance($instance);
}

/**
 * Add roles fields to the og_membership entity.
 */
function party_og_update_7200() {
  $entity_info = entity_get_info('og_membership');

  if (!field_info_field('party_og_membership_roles')) {
    $field = array(
      'field_name' => 'party_og_membership_roles',
      'type' => 'list_integer',
      'cardinality' => FIELD_CARDINALITY_UNLIMITED,
      'settings' => array(
        'allowed_values_function' => 'party_og_membership_roles_allowed_values',
      ),
    );
    field_create_field($field);
  }

  foreach (array_keys($entity_info['bundles']) as $bundle) {
    if (!field_info_instance('og_membership', 'party_og_membership_roles', $bundle)) {
      $instance = array(
        'field_name' => 'party_og_membership_roles',
        'entity_type' => 'og_membership',
        'bundle' => $bundle,
        'label' => t('Membership Roles'),
        'description' => t('The roles this party has as a member of the group.'),
        'widget' => array(
          'type' => 'options_buttons',
        ),
      );
      field_create_instance($instance);
    }
  }

  // Make Parties groups.
  $og_field = og_fields_info('group_group');
  og_create_field('group_group', 'party', 'party', $og_field);
}

/**
 * Add OG Audience Field to Users
 */
function party_og_update_7201() {
  $field_def = og_fields_info(OG_AUDIENCE_FIELD);
  $field = $field_def['field'];
  $field['field_name'] = 'party_og_user_audience';
  $field['settings']['target_type'] = 'party';
  if (!field_info_field($field['field_name'])) {
    field_create_field($field);
  }

  $instance = $field_def['instance'];
  $instance['field_name'] = 'party_og_user_audience';
  $instance['entity_type'] = 'user';
  $instance['bundle'] = 'user';
  $instance['label'] = t('Party Audience');
  if (!field_info_instance('user', 'party_og_user_audience', 'user')) {
    field_create_instance($instance);
  }
}
