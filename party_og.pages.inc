<?php
/**
 * @file
 * Page definitions for the party og module.
 */

/**
 * Page callback for the edit modal popup page.
 */
function party_og_edit_og_membership($js, $membership = NULL) {
  if (!$membership) {
    return MENU_NOT_FOUND;
  }

  $party = party_load($membership->etid);
  $conf['group_id'] = $membership->gid;
  $conf['group_audience_field_name'] = $membership->field_name;
  $conf['show_member_roles'] = TRUE;

  if ($js) {
    // This is in a modal, so let's do the modal magic.
    ctools_include('ajax');
    ctools_include('modal');

    $form_state = array(
      'ajax' => TRUE,
      'build_info' => array(
        'args' => array($party, $conf),
      ),
    );
    $commands = ctools_modal_form_wrapper('party_og_party_add_to_group_form', $form_state);

    if (!empty($form_state['executed'])) {
      // The form has been executed, so let's redirect to the destination page.
      $commands = array();
      $commands[] = ctools_modal_command_dismiss();
      $commands[] = ctools_ajax_command_reload();
    }

    print ajax_render($commands);
    ajax_footer();
    return;
  }
  else {
    // This is just a page, so we don't need to worry.
    return drupal_get_form('party_og_party_add_to_group_form', $party, $conf);
  }
}

/**
 * Page callback for removing memberships.
 */
function party_og_remove_og_membership($js, $membership = NULL) {
  if (!$membership) {
    return MENU_NOT_FOUND;
  }

  if ($js) {
    // This is in a modal, so let's do the modal magic.
    ctools_include('ajax');
    ctools_include('modal');

    $form_state = array(
      'ajax' => TRUE,
      'build_info' => array(
        'args' => array($membership),
      ),
    );
    $commands = ctools_modal_form_wrapper('party_og_remove_og_membership_form', $form_state);

    if (!empty($form_state['executed'])) {
      // The form has been executed, so let's redirect to the destination page.
      $commands = array();
      $commands[] = ctools_modal_command_dismiss();
      $commands[] = ctools_ajax_command_reload();
    }

    print ajax_render($commands);
    ajax_footer();
    return;
  }
  else {
    // This is just a page, so we don't need to worry.
    return drupal_get_form('party_og_remove_og_membership_form', $membership);
  }
}

/**
 * Confrimation form for removing memberships.
 */
function party_og_remove_og_membership_form($form, &$form_state, $membership) {
  $form['#membership'] = $membership;

  $form['#attributes']['class'][] = 'confirmation';
  $form['description'] = array('#markup' => t('Are you sure you want to remove this relationship?'));

  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Remove Relationship'),
    '#submit' => array(
      'party_og_remove_og_membership_form_confirm',
    ),
  );
  $form['actions']['cancel'] = array(
    '#type' => 'submit',
    '#value' => t('Cancel'),
    '#submit' => array(),
  );
  // By default, render the form using theme_confirm_form().
  if (!isset($form['#theme'])) {
    $form['#theme'] = 'confirm_form';
  }

  return $form;
}

/**
 * Submit callback.
 */
function party_og_remove_og_membership_form_confirm($form, &$form_state) {
  $membership = $form['#membership'];
  og_ungroup($membership->group_type, $membership->gid, $membership->entity_type, $membership->etid);
}
