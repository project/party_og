<?php
/**
 * @file
 * Contains PartyOgElementAddToGroup class.
 */

/**
 * Class for the Party OG Add Group to Party form.
 */
class PartyOgElementAddToGroup extends FlexiformElement {

  /**
   * {@inheritdoc}
   */
  public function configureForm($form, &$form_state, $flexiform) {
    $form = parent::configureForm($form, $form_state, $flexiform);

    $form['required'] = array(
      '#type' => 'checkbox',
      '#title' => t('Required'),
      '#default_value' => !empty($this->settings['required']),
    );

    $form['edit_mode'] = array(
      '#type' => 'checkbox',
      '#title' => t('Edit existing relationship'),
      '#description' => t('Expose an edit form for an existing relationship if it already exists.'),
      '#default_value' => !empty($this->settings['edit_mode']),
    );

    $form['edit_mode_change'] = array(
      '#type' => 'checkbox',
      '#title' => t('Allow changing of the organisation'),
      '#default_value' => !empty($this->settings['edit_mode_change']),
      '#states' => array(
        'visible' => array(
          ':input[name="edit_mode"]' => array('checked' => TRUE),
        ),
      ),
    );

    $form['autocomplete_label'] = array(
      '#type' => 'textfield',
      '#title' => t('Group Label'),
      '#description' => t('The Label to go on the Group auto-complete element'),
      '#default_value' => !empty($this->settings['autocomplete_label']) ? $this->settings['autocomplete_label'] : '',
    );

    $form['autocomplete_desc'] = array(
      '#type' => 'textarea',
      '#title' => t('Group Description'),
      '#description' => t('The help text to got underneat the group auto-complete element'),
      '#default_value' => !empty($this->settings['autocomplete_desc']) ? $this->settings['autocomplete_desc'] : '',
    );

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function configureFormSubmit($form, &$form_state, $flexiform) {
    $this->settings['required'] = $form_state['values']['required'];
    $this->settings['edit_mode'] = $form_state['values']['edit_mode'];
    $this->settings['edit_mode_change'] = $form_state['values']['edit_mode_change'];
    $this->settings['autocomplete_label'] = $form_state['values']['autocomplete_label'];
    $this->settings['autocomplete_desc'] = $form_state['values']['autocomplete_desc'];

    parent::configureFormSubmit($form, $form_state, $flexiform);
  }

  /**
   * {@inheritdoc}
   */
  public function form($form, &$form_state, $entity) {
    // Set up the element.
    $form[$this->element_namespace]['#parents'] = $form['#parents'];
    $form[$this->element_namespace]['#parents'][] = $this->element_namespace;
    $form[$this->element_namespace]['#tree'] = TRUE;

    if ($this->settings['label'] && empty($this->settings['display_options']['hide_label'])) {
      $form[$this->element_namespace]['#title'] = $this->settings['label'];
      $form[$this->element_namespace]['#type'] = 'fieldset';
    }

    // Pass onto the form builder.
    if (!empty($this->settings['edit_mode']) && !empty($entity->{$this->element_info['field_name']}[LANGUAGE_NONE][0]['target_id'])) {
      $conf['group_id'] = $entity->{$this->element_info['field_name']}[LANGUAGE_NONE][0]['target_id'];
    }
    $conf['group_audience_field_name'] = $this->element_info['field_name'];
    $form[$this->element_namespace] = party_og_party_add_to_group_form($form[$this->element_namespace], $form_state, $entity, $conf);

    // Make some alterations to the form.
    unset($form[$this->element_namespace]['submit']);

    $form[$this->element_namespace]['group']['#required'] = !empty($this->settings['required']);

    if (!empty($conf['group_id']) && $this->settings['edit_mode_change']) {
      $form[$this->element_namespace]['group']['#disabled'] = FALSE;
      $form[$this->element_namespace]['group_original'] = array(
        '#type' => 'value',
        '#value' => $conf['group_id'],
      );
    }

    if (!empty($this->settings['autocomplete_label'])) {
      $form[$this->element_namespace]['group']['#title'] = $this->settings['autocomplete_label'];
    }

    if (!empty($this->settings['autocomplete_desc'])) {
      $form[$this->element_namespace]['group']['#description'] = $this->settings['autocomplete_desc'];
    }

    $form = parent::form($form, $form_state, $entity);
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function formValidate($form, &$form_state, $entity) {
    $values = $this->formExtractValues($form, $form_state, $entity);
    if (!empty($values['group']) && (!is_numeric($values['group']) || !party_load($values['group']))) {
      form_error($form[$this->element_namespace]['group'], t('Invalid group selected. Please select from the suggestions provided.'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function formSubmit($form, &$form_state, $entity) {
    $values = $this->formExtractValues($form, $form_state, $entity);
    if (empty($values['group']) && empty($values['group_original'])) {
      return;
    }

    // Save the new details.
    $entity->{$this->element_info['field_name']}[LANGUAGE_NONE][] = array('target_id' => $values['group']);
    entity_save($this->entity_type, $entity);

    $field_info = field_info_field($this->element_info['field_name']);
    $membership = og_get_membership(
                    $field_info['settings']['target_type'],
                    $values['group'],
                    $this->entity_type,
                    entity_id($this->entity_type, $entity)
                  );
    field_attach_submit('og_membership', $membership, $form[$this->element_namespace], $form_state);
    $membership->save();

    // Remove any old group.
    if (!empty($values['group_original']) && $values['group_original'] != $values['group']) {
      $old_membership = og_get_membership(
        $field_info['settings']['target_type'],
        $values['group_original'],
        $this->entity_type,
        entity_id($this->entity_type, $entity)
      );
      if ($old_membership) {
        $old_membership->delete();
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function formExtractValues($form, &$form_state, $entity) {
    $parents = $form['#parents'];
    $parents[] = $this->getEntityNamespace();
    $parents[] = $this->element_namespace;

    return drupal_array_get_nested_value($form_state['values'], $parents);
  }
}
