<?php
/**
 * @file
 * Views handlers for the mbership fields.
 */

/**
 * Implements hook_views_data_alter().
 */
function party_og_views_data_alter(&$data) {
  if (empty($data['og_membership'])) {
    return;
  }

  // Add the forms for all membership roles.
  $entity_info = entity_get_info('og_membership');
  $field = field_info_field('party_og_membership_roles');
  $roles = array();
  foreach (array_keys($entity_info['bundles']) as $bundle) {
    $instance = field_info_instance('og_membership', 'party_og_membership_roles', $bundle);

    if ($instance) {
      $entity = (object) array(
        'group_type' => 'party',
        'group_bundle' => 'party',
      );

      $roles += party_og_membership_roles_allowed_values($field, $instance, 'og_membership', $entity);
    }
  }

  foreach ($roles as $rid => $role) {
    $field = array(
      'title' => t('OG Role checkbox for the @role role', array('@role' => $role)),
      'help' => t('Provide a checkbox for give a certain party the @role role.', array('@role' => $role)),
      'real field' => 'id',
      'field' => array(
        'handler' => 'party_og_handler_field_membership_role_checkbox',
        'click sortable' => FALSE,
        'og_role' => $role,
      ),
    );
    $data['og_membership']['og_membership_role_checkbox__' . $role] = $field;
  }

  // Membership edit modal link.
  $data['og_membership']['edit_modal'] = array(
    'title' => t('Edit Link (Modal)'),
    'help' => t('Show a link to edit the membership in a modal popup.'),
    'field' => array(
      'real field' => 'id',
      'handler' => 'party_og_handler_membership_modal_edit_link',
    ),
  );

  // Membership remove modal link.
  $data['og_membership']['remove_modal'] = array(
    'title' => t('Remove Link (Modal)'),
    'help' => t('Show a link to remove the membership in a modal popup.'),
    'field' => array(
      'real field' => 'id',
      'handler' => 'party_og_handler_membership_modal_remove_link',
    ),
  );
}
