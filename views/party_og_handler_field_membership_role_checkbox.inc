<?php

/**
 * @file
 * Definition of clinks_party_handler_field_og_manager.
 * Based on core Views' views_handler_field_custom.inc.
 */

/**
 * A handler to provide a field for OG manager checkbox.
 */
class party_og_handler_field_membership_role_checkbox extends views_handler_field {
  /**
   * Overrides views_handler_field::construct().
   */
  function construct() {
    parent::construct();

    $this->additional_fields['id'] = 'id';
    $this->additional_fields['group_type'] = 'group_type';
    $this->additional_fields['gid'] = 'gid';
    $this->additional_fields['entity_type'] = 'entity_type';
    $this->additional_fields['etid'] = 'etid';
  }

  function query() {
    $this->ensure_my_table();
    $this->add_additional_fields();
  }

  function option_definition() {
    $options = parent::option_definition();
    return $options;
  }

  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);
  }

  function render($values) {
    return '<!--form-item-' . $this->form_element_name() . '--' . $this->view->row_index . '-->';
  }

  function form_element_name() {
    // Make sure this value is unique for all the view fields
    return str_replace(' ', '-', $this->options['id']);
  }

  function form_element_row_id($row_id) {
    // You could use values from $this->view->result[$row_id]
    // to provide complex row ids.
    return $row_id;
  }

  function views_form(&$form, &$form_state) {
    // The view is empty, abort.
    if (empty($this->view->result)) {
      return;
    }

    // Set form action to request_uri() explicitly. Otherwise it will be
    // something like 'admin/party/235/235' due to party dashboard's original
    // path.
    $form['#action'] = request_uri();

    $field_name = $this->form_element_name();
    $form[$field_name] = array(
      '#tree' => TRUE,
    );

    // At this point, the query has already been run, so we can access the results
    foreach ($this->view->result as $row_id => $row) {
      $group_type = $row->{$this->aliases['group_type']};
      $gid = $row->{$this->aliases['gid']};
      $id = $row->{$this->aliases['id']};

      $group = entity_load_single($group_type, $gid);
      // This is not a group entity, abort.
      if (!og_is_group($group_type, $group)) {
        continue;
      }

      $form_element_row_id = $this->form_element_row_id($row_id);

      $form[$field_name][$form_element_row_id] = array(
        '#type' => 'checkbox',
        '#return_value' => 1,
        '#title' => $this->definition['og_role'],
        '#default_value' => $this->has_role($id, $this->definition['og_role']),
        '#attributes' => array('class' => array('party-management-og-manager')),
        '#group_type' => $group_type,
        '#group_id' => $gid,
      );
    }
  }

  // Submit handler of views_form().
  function views_form_submit($form, &$form_state) {
    $field_name = $this->form_element_name();

    foreach ($form_state['values'][$field_name] as $row_id => $value) {
      // Only take action if the checkbox has changed.
      if ($form[$field_name][$row_id]['#default_value'] == $value) {
        continue;
      }

      $element = &$form[$field_name][$row_id];

      $id = $this->view->result[$row_id]->{$this->aliases['id']};
      $og_membership = entity_load_single('og_membership', $id);
      $role_id = $this->get_role_id($element['#group_type'], $element['#group_id'], $this->definition['og_role']);

      // Add the role to the membership.
      if ($value === 1) {
        $og_membership->party_og_membership_roles[LANGUAGE_NONE][] = array('value' => $role_id);
      }
      // Remove the role
      elseif ($value === 0) {
        foreach ($og_membership->party_og_membership_roles[LANGUAGE_NONE] as $delta => $item) {
          if ($item['value'] == $role_id) {
            break;
          }
        }

        unset($og_membership->party_og_membership_roles[LANGUAGE_NONE][$delta]);
        $og_membership->party_og_membership_roles[LANGUAGE_NONE] = array_values( $og_membership->party_og_membership_roles[LANGUAGE_NONE]);
      }
      entity_save('og_membership', $og_membership);
    }
  }

  // Check if a given user is member of the group and has the group admin role
  // as well.
  private function has_role($membership_id, $role) {
    $query = db_select('og_membership', 'ogm');
    $query->join('field_data_party_og_membership_roles', 'ogmr', 'ogmr.entity_type = :entity_type AND ogm.id = ogmr.entity_id AND ogmr.deleted = 0', array(':entity_type' => 'og_membership'));
    $query->join('og_role', 'ogr', 'ogmr.party_og_membership_roles_value = ogr.rid');
    $query->condition('ogm.id', $membership_id);
    $query->condition('ogr.name', $role);

    $count = $query->countQuery()->execute()->fetchField();

    if ($count) {
      return TRUE;
    }
    return FALSE;
  }

  private function get_role_id($group_type, $gid, $role) {
    $roles = og_roles($group_type, FALSE, $gid);
    $roles = array_flip($roles);
    return $roles[$role];
  }
}
