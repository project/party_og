<?php

/**
 * @file
 * CTools content type for a clinks relationship edit form
 */

/**
 * Plugins are described by creating a $plugin array which will be used
 * by the system that includes this file.
 */
$plugin = array(
  'title' => t('Add any Party to a Group Form'),
  'content_types' => 'party_og_group_add_a_party_form',
  // 'single' means not to be subtyped.
  'single' => TRUE,
  // Name of a function which will render the block.
  'render callback' => 'party_og_group_add_a_party_form_render',

  // Icon goes in the directory with the content type.
  'description' => t('Show an add to group form.'),
  'required context' => new ctools_context_required(t('Party'), 'entity:party'),
  'edit form' => 'party_og_group_add_a_party_form_edit_form',
  'admin title' => 'party_og_group_add_a_party_form_admin_title',

  // presents a block which is used in the preview of the data.
  // Pn Panels this is the preview pane shown on the panels building page.
  'category' => array(t('Party'), 0),
);

/**
 * Render the add Party to Group Form.
 *
 * @param $subtype
 * @param $conf
 *   Configuration as done at admin time
 * @param $args
 * @param $context
 *   Context - in this case we don't have any
 *
 * @return
 *   An object with at least title and content members
 */
function party_og_group_add_a_party_form_render($subtype, $conf, $args, $context) {
  $block = new stdClass();
  $block->title = t('Add a party.');
  $block->content = '';

  if (!empty($context->data)) {
    $group = $context->data;

    $form = party_og_group_add_a_party_form_wrapper($group, $conf);

    // Render it...
    $block->content = drupal_render($form);
  }

  return $block;
}

/**
 * Config Form
 */
function party_og_group_add_a_party_form_edit_form($form, &$form_state) {
  $fields = og_get_group_audience_fields('party', 'party');

  if (count($fields) > 1) {
    $form['group_audience_field_name'] = array(
      '#type' => 'select',
      '#title' => t('Group Audience Field'),
      '#descritpion' => t('Which group audience field should be used on the party?'),
      '#options' => $fields,
      '#default_value' => !empty($form_state['conf']['group_audience_field_name']) ? $form_state['conf']['group_audience_field_name'] : NULL,
    );
  }
  elseif (count($fields)) {
    $form['group_audience_field_name'] = array(
      '#type' => 'value',
      '#value' => key($fields),
    );
  }
  else {
    $form['group_audience_field_name'] = array(
      '#type' => 'value',
      '#value' => FALSE,
    );
  }

  if (module_exists('party_hat')) {
    $hats = entity_load('party_hat');

    $options = array();
    foreach ($hats as $hat) {
      $options[$hat->name] = $hat->label;
    }

    $form['hats'] = array(
      '#title' => t('Restrict by hats'),
      '#description' => t("Autocomplete field's suggestions can be restricted by party hats."),
      '#type' => 'select',
      '#options' => $options,
      '#default_value' => !empty($form_state['conf']['hats']) ? $form_state['conf']['hats'] : array(),
      '#multiple' => TRUE,
    );
  }

  $form['member_label'] = array(
    '#title' => t('Member Field Label'),
    '#type' => 'textfield',
    '#default_value' => !empty($form_state['conf']['member_label']) ? $form_state['conf']['member_label'] : 'Member',
  );

  $form['show_member_roles'] = array(
    '#title' =>  t('Show Membership Roles Field'),
    '#type' => 'checkbox',
    '#default_value' => !empty($form_state['conf']['show_member_roles']) ? $form_state['conf']['show_member_roles'] : FALSE,
  );

  return $form;
}

function party_og_group_add_a_party_form_edit_form_submit(&$form, &$form_state) {
  foreach (element_children($form) as $key) {
    if (!empty($form_state['values'][$key])) {
      $form_state['conf'][$key] = $form_state['values'][$key];
    }
  }
}

/**
 * Title Callback
 */
function party_og_group_add_a_party_form_admin_title($subtype, $conf, $context = NULL) {
  return "Add Party to a Group Form";
}
